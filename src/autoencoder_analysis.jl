import Flux

#TODO: Better documentation
#TODO: Expose more parameters

"""
    diagm(N::Integer)

Create a diagonal matrix.
"""
diagm(N::Integer) = (A = zeros(N, N); A[CartesianIndex.(1:N, 1:N)] .+= 1; A)

"""
    build_ann_model_ae(imgsize::Tuple,
		N_modes::Integer;
		actfun::Function = Flux.leakyrelu) -> (encoder, decoder)

Create a dense ANN AE network model.
"""
function build_ann_model_ae(
    imgsize::Tuple,
    N_modes::Integer;
    actfun::Function = Flux.leakyrelu,
)
    N_x = prod(imgsize)
    encoder = Flux.Chain(
        # Flatten the near-field images
        Flux.flatten,
        # Make a 3-layer network with N -> N/2 -> N/4 -> M size
        Flux.Dense(N_x, div(N_x, 2), actfun),
        Flux.Dense(div(N_x, 2), div(N_x, 4), actfun),
        Flux.Dense(div(N_x, 4), N_modes),
    )
    decoder = Flux.Chain(
        # Simple 2-layer network with M -> N/2 -> N size
        Flux.Dense(N_modes, div(N_x, 2), actfun),
        Flux.Dense(div(N_x, 2), N_x, actfun),
        # Reshape into the original near-field image dimensions
        x -> reshape(x, imgsize...),
    )
    encoder, decoder
end

"""
    build_cnn_model_ae(
		imgsize::Tuple,
		N_modes::Integer;
		actfun::Function = Flux.leakyrelu,
	) -> (encoder, decoder)

Create a CNN network model.
"""
function build_cnn_model_ae(
    imgsize::Tuple,
    N_modes::Integer;
    actfun::Function = Flux.leakyrelu,
)
    # Determine the output size of the convolution layers (depends on
    # layers, convolution and pooling sizes) 
    cnn_output_size = replace(Int.(floor.([imgsize[1:end-1] ./ 8..., 4])), 0 => 1)

    # Size of the image in pixels
    N_x = prod(imgsize)

    # Determine the correct size/dimension of the various convolution layers
    convsize = Tuple(fill(3, length(imgsize) - 2))
    padsize = Tuple(fill(1, length(imgsize) - 2))
    poolsize = Tuple(fill(2, length(imgsize) - 2))
    poolpadsize = Tuple(fill(0, length(imgsize) - 2))

    encoder = Flux.Chain(
        # 3-layer convolutional network
        Flux.Conv(convsize, imgsize[end] => 16, pad = padsize, actfun),
        Flux.MaxPool(poolsize, pad = poolpadsize),
        Flux.Conv(convsize, 16 => 8, pad = padsize, actfun),
        Flux.MaxPool(poolsize, pad = poolpadsize),
        Flux.Conv(convsize, 8 => 4, pad = padsize, actfun),
        Flux.MaxPool(poolsize, pad = poolpadsize),
        # Flatten the output of the convolutional layers
        Flux.flatten,
        # Dense layer from convolution output to modes
        Flux.Dense(prod(cnn_output_size), N_modes),
    )
    decoder = Flux.Chain(
        # Simple 2-layer network with M -> N/2 -> N size
        Flux.Dense(N_modes, div(N_x, 2), actfun),
        Flux.Dense(div(N_x, 2), N_x, actfun),
        # Reshape into the original near-field image dimensions
        x -> reshape(x, imgsize..., :),
    )
    encoder, decoder
end

"""
    function train_network(
		N_modes::Integer,
		dataX::AbstractArray,
		dataY::AbstractArray,
		dataX_test::AbstractArray,
		dataY_test::AbstractArray;
		N_epoch::Integer = 1000,
		actfun::Function = Flux.leakyrelu,
		comptype::Function = Flux.cpu,
		loss_threshold::Number = 1e-5,
		nettype = :cnn,
		modal_recovery::Bool = true,
	)

Create and train a ML network in modal decomposition and, optionally,
modal recovery.
"""
function train_network(
    N_modes::Integer,
    dataX::AbstractArray,
    dataY::AbstractArray,
    dataX_test::AbstractArray,
    dataY_test::AbstractArray;
    N_epoch::Integer = 1000,
    actfun::Function = Flux.leakyrelu,
    comptype::Function = Flux.cpu,
    loss_threshold::Number = 1e-5,
    nettype = :cnn,
    modal_recovery::Bool = true,
)
    @assert nettype in [:ann, :cnn]
    @assert length(dataX) == length(dataY)
    @assert length(dataX_test) == length(dataY_test)

    # Generate the correct model (ANN or CNN based autoencoder)
    (encoder, decoder) =
        nettype == :cnn ? build_cnn_model_ae(size(dataX[1]), N_modes, actfun = actfun) :
        build_ann_model_ae(size(dataX[1]), N_modes, actfun = actfun)

    # Move networks to either CPU or GPU
    encoder = encoder |> comptype
    decoder = modal_recovery ? (decoder |> comptype) : nothing

    # Define loss functions for modal decomposition, near-field
    # reconstruction, and total loss
    #TODO: Consider adding noise to input for loss calculation to encourage noise resilience
    loss_nf(X, Y) = modal_recovery ? Flux.mse(X, decoder(encoder(X))) : 0.0
    loss_pow(X, Y) = Flux.mse(Y, encoder(X))
    loss(X, Y) = modal_recovery ? loss_nf(X, Y) + loss_pow(X, Y) : loss_pow(X, Y)

    # Optimization function for learning
    opt = Flux.ADAM()

    # Define various iteration variables
    # This should be greater than any obtained loss
    best_loss = Inf
    # Store epoch of last improvement in loss
    last_improvement = 0
    # Store the best models
    best_model = modal_recovery ? (encoder, decoder) : encoder

    @info string("Beginning training loop...")
    for epoch_idx = 1:N_epoch
        # Single iteration of training
        p = modal_recovery ? Flux.params(encoder, decoder) : Flux.params(encoder)
        Flux.train!(loss, p, zip(dataX, dataY), opt)

        # Calculate accuracy
        nfloss, powloss =
            Flux.mean([loss_nf(dataX_test[i], dataY_test[i]) for i = 1:length(dataX_test)]),
            Flux.mean([
                loss_pow(dataX_test[i], dataY_test[i]) for i = 1:length(dataX_test)
            ])
        totalloss = nfloss + powloss

        # Print progress
        @info string(
            "Epoch #",
            epoch_idx,
            ": NF=",
            nfloss,
            ", Pow=",
            powloss,
            ", Total=",
            totalloss,
        )

        # If our accuracy is good enough, quit out.
        if totalloss <= loss_threshold
            @info string(
                " -> Early-exiting: We reached our target loss of ",
                loss_threshold,
            )
            break
        end

        # If this is the best accuracy we've seen so far, save the models
        if totalloss < best_loss
            #TODO: save as msgpack?
            @info string(" -> New best accuracy!")
            #BSON.@save "ae_model.bson" params = Flux.cpu.(Flux.params(encoder, decoder)) epoch_idx totalloss
            best_model =
                modal_recovery ? (deepcopy(encoder), deepcopy(decoder)) : deepcopy(encoder)
            best_loss = totalloss
            last_improvement = epoch_idx
        end

        # If we haven't seen improvement in 5 epochs, drop our learning rate
        if epoch_idx - last_improvement >= 10 && opt.eta > 1e-6
            opt.eta /= 10.0
            @warn string(" -> Haven't improved in a while, dropping learning rate to $(opt.eta)!")

            # After dropping learning rate, give it a few epochs to improve
            last_improvement = epoch_idx
        end

        # If there hasn't been progress in a long while, consider network converged
        if epoch_idx - last_improvement >= 30
            @warn string(" -> We're calling this converged.")
            break
        end
    end

    # Return the best model
    #TODO: give some feedback with regard to performance?
    best_model
end

"""
    modalanalysis(
		modepowers::AbstractMatrix,
		nearfields::AbstractArray;
		N_epoch::Integer = 1000,
		actfun::Function = Flux.leakyrelu,
		comptype = :cpu,
		splitdataset::Bool = false,
		nettype = :cnn,
		modal_recovery::Bool = true,
	) -> (network, modes)

Train artificial neural network in modal decomposition and, if
`modal_recovery` is `true`, recover the principal modal profiles.
"""
function modalanalysis(
    modepowers::AbstractMatrix,
    nearfields::AbstractArray;
    N_epoch::Integer = 1000,
    actfun::Function = Flux.leakyrelu,
    comptype = :cpu,
    splitdataset::Bool = false,
    nettype = :cnn,
    modal_recovery::Bool = true,
)
    @assert comptype in [:cpu, :gpu]
    @assert nettype in [:cnn, :ann]
    @assert size(modepowers, 2) == size(nearfields)[end]

    # Move the inputs to either CPU or GPU
    comptype = comptype == :cpu ? Flux.cpu : Flux.gpu
    modepowers = modepowers |> comptype
    nearfields = nearfields |> comptype

    N_modes = size(modepowers, 1)

    # Convert input matrices to arrays of images/powers
    dataX = collect(eachslice(nearfields, dims = ndims(nearfields)))
    dataY = collect(eachslice(modepowers, dims = ndims(modepowers)))

    # Reshape into appropriate shape for CNN-based AE
    dataX = reshape.(dataX, size(dataX[1])..., 1, 1)

    # Optionally, plit the input into training and testing datasets
    dataX, dataX_test = splitdataset ? (dataX[1:2:end], dataX[2:2:end]) : (dataX, dataX)
    dataY, dataY_test = splitdataset ? (dataY[1:2:end], dataY[2:2:end]) : (dataY, dataY)

    # Generate and train the networks
    mlres = train_network(
        N_modes,
        dataX,
        dataY,
        dataX_test,
        dataY_test,
        N_epoch = N_epoch,
        actfun = actfun,
        comptype = comptype,
        nettype = nettype,
        modal_recovery = modal_recovery,
    )

    # Remove the training/testing datasets
    dataX = dataX_test = dataY = dataY_test = nothing
    GC.gc()

    if modal_recovery
        encoder, decoder = mlres
        # Recover the mode profiles using the decoder and the standard basis vectors
        INm = diagm(N_modes) |> comptype
        modes_R =
            reduce(hcat, [decoder(Flux.unsqueeze(x, 2)) for x in eachslice(INm, dims = 2)])
        modes_R = reshape(modes_R, size(nearfields)[1:end-1]..., N_modes)

        # Move the outputs back to the CPU
        modes_R = modes_R |> Flux.cpu
        encoder = encoder |> Flux.cpu
        return encoder, modes_R
    else
        encoder = mlres
        # Move the outputs back to the CPU
        encoder = encoder |> Flux.cpu
        return encoder
    end
end
