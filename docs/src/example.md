# Basic Usage

The functionality of this package is provided by the `modalanalysis`
function. This function two arguments, a collection of near-field images
(either 1D or 2D images) as well as the corresponding modal power
decomposition information. By default, the function will return a
neural network capable of modal decomposition as well as the an array
of the recovered modal near-fields. The basic usage for 1D images is
as follows:

```julia
import ModeAnalysisML

# Load images as an 2D array `nearfields` of size (PIXELS, IMAGES)
# Load modal power coefficients as a 2D array `modalpowers` of size (MODES, IMAGES)

# We obtain a neural network `modaldecomposer` capable of modal decomposition
# and modal profiles as a 2D array `modeprofiles` of size (PIXELS, IMAGES)
modaldecomposer, modeprofiles=ModeAnalysisML.modalanalysis(
	modalpowers,
	nearfields,
)

# We use the neural network to estimate the modal power coefficients
# for a set of near-field images (must be a 2D array of size (PIXELS, IMAGES))
modaldecomposition=modaldecomposer(nearfields)
```

The code will similarly work on 2D images:

```julia
# Load images as an 3D array `nearfields` of size (PIXELSX, PIXELSY, IMAGES)
# Load modal power coefficients as a 2D array `modalpowers` of size (MODES, IMAGES)

# We obtain a neural network `modaldecomposer` capable of modal decomposition
# and modal profiles as a 3D array `modeprofiles` of size (PIXELSX, PIXELSY, IMAGES)
modaldecomposer, modeprofiles=ModeAnalysisML.modalanalysis(
	modalpowers,
	nearfields,
)

# We use the neural network to estimate the modal power coefficients
# for a set of near-field images (must be a 3D array of size (PIXELSX, PIXELSY, IMAGES))
modaldecomposition=modaldecomposer(nearfields)
```

# Advanced Options

By default this package uses an autoencoder to simultaneously train a
network in modal decomposition and perform modal profile recovery. The
following options can be used to modify this behavior.

`N_epoch` controls the (maximal) number of network training epochs. This
number should be increased to increase network accuracy.

`actfun` is an activation function used by the neural network.

`comptype` may be either `:cpu` or `:gpu` to specify whether network
training should be done on the CPU or a GPU. Note that the `:gpu`
option will require appropriate hardware and software packages to be
installed.

`splitdataset` dictates whether or not the provided dataset should be
split in half into separate training and testing dataset.

`nettype` may be either `:ann` or `:cnn` to specify whether a dense
encoder network should be used or a convolutional neural network based
encoder network.

`modal_recovery` dictates whether or not the function should attempt
modal recovery. Setting `modal_recovery=true` will simplify the network
training by ignoring the decoder component of the autoencoder, improving
performance. It will also *not* return the `modeprofiles` recovered
modal profiles. This option may be good if you already know or don't
care to know the modal profiles. 
