# ModeAnalysisML.jl Documentation

## About

`ModeAnalysisML` implements autoencoder-based modal analysis. Provided
a training set of multi-moded near-fields with modal power coefficients
the autoencoder will recovery the modal profiles and return a network
capable of modal decomposition.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl
```

## Features

* Modal profile recovery and modal decomposition
* 1D and 2D image support
* Implements conventional and convolutional autoencoders

## Related Packages

* [ModeAnalysis.jl](https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl) for conventional methods of modal analysis
* [LASERAnalysis.jl](https://gitlab.com/pawelstrzebonski/LASERAnalysis.jl) for basic laser analysis, including identifying and clustering spectral modes for series of optical spectra
