@testset "autoencoder_analysis.jl - 1D Modes" begin
    x = 0:0.05:1
    modes = Float32.(reduce(hcat, [sin.(x .* pi .* i) .^ 2 for i = 1:3]))
    powers = Float32.(rand(3, 10))
    nfs = modes * powers

    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            splitdataset = false,
            modal_recovery = true,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            splitdataset = false,
            modal_recovery = true,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            splitdataset = true,
            modal_recovery = true,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            splitdataset = true,
            modal_recovery = true,
        );
        true
    )

    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            modal_recovery = false,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            modal_recovery = false,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            splitdataset = true,
            modal_recovery = false,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            splitdataset = true,
            modal_recovery = false,
        );
        true
    )
end

@testset "autoencoder_analysis.jl - 2D Modes" begin
    N = 3
    x = y = 0:0.05:1
    modes =
        Float32.(reshape(
            reduce(
                hcat,
                [[sin(x * pi * i)^2 * sin(y * pi * i)^2 for x in x, y in y] for i = 1:N],
            ),
            length(x),
            length(y),
            N,
        ))
    powers = Float32.(rand(N, 10))
    nfs = reshape(reshape(modes, :, N) * powers, size(modes)[1:end-1]..., :)

    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            splitdataset = false,
            modal_recovery = true,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            splitdataset = false,
            modal_recovery = true,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            splitdataset = true,
            modal_recovery = true,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            splitdataset = true,
            modal_recovery = true,
        );
        true
    )

    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            modal_recovery = false,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            modal_recovery = false,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :ann,
            splitdataset = true,
            modal_recovery = false,
        );
        true
    )
    @test (
        ModeAnalysisML.modalanalysis(
            powers,
            nfs,
            N_epoch = 10,
            nettype = :cnn,
            splitdataset = true,
            modal_recovery = false,
        );
        true
    )
end
