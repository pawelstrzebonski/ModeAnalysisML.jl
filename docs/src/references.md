# References

This code implements the methods discussed in the IPC 2020 work:

[P. Strzebonski and K. Choquette, "Machine Learning for Modal Analysis,"
presented at the 2020 IEEE Photonics Conference (IPC), Oct. 2020](https://doi.org/10.1109/ipc47351.2020.9252555)

If you use this package for your research, we would appreciate it if
you would cite the paper.

```
@inproceedings{Strzebonski2020,
  doi = {10.1109/ipc47351.2020.9252555},
  url = {https://doi.org/10.1109/ipc47351.2020.9252555},
  year = {2020},
  month = sep,
  publisher = {{IEEE}},
  author = {Pawel Strzebonski and Kent Choquette},
  title = {Machine Learning for Modal Analysis},
  booktitle = {2020 {IEEE} Photonics Conference ({IPC})}
}
```
