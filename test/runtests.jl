import ModeAnalysisML
import Test: @test_broken, @test, @test_throws, @testset

tests = ["autoencoder_analysis"]

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
