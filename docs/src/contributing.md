# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Better network design and training defaults
* Create and expose more network configuration options

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support both 1D and 2D images
* We aim to support both CPU and GPU compute
* We aim to minimize the amount of code and function repetition when implementing the above features
