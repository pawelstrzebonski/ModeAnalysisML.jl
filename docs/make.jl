import ModeAnalysisML
using Documenter

makedocs(;
    modules = [ModeAnalysisML],
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "ModeAnalysisML.jl"),
    authors = "Pawel Strzebonski <6020899-pawelstrzebonski@users.noreply.gitlab.com> and contributors",
    sitename = "ModeAnalysisML.jl",
    format = Documenter.HTML(;
        prettyurls = get(ENV, "CI", "false") == "true",
        canonical = "https://pawelstrzebonski.gitlab.io/ModeAnalysisML.jl",
        assets = String[],
    ),
    pages = [
        "Home" => "index.md",
        "Example Usage" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["autoencoder_analysis.jl" => "autoencoder_analysis.md"],
        "test/" => ["autoencoder_analysis.jl" => "autoencoder_analysis_test.md"],
    ],
)
