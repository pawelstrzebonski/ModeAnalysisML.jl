# ModeAnalysisML

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/ModeAnalysisML.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl/commits/master)

## About

`ModeAnalysisML` implements autoencoder-based modal analysis. Provided
a training set of multi-moded near-fields with modal power coefficients
the autoencoder will recovery the modal profiles and return a network
capable of modal decomposition.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl
```
