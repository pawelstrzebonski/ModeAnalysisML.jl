# autoencoder_analysis.jl

## Description

These functions create and train autoencoder neural networks where
the encoder performs modal decomposition of multi-moded near-field
images and the decoder predicts the multi-moded near-field images from
the modal power coefficients.

## Functions

```@autodocs
Modules = [ModeAnalysisML]
Pages   = ["autoencoder_analysis.jl"]
```
